package main

import (
	"database/sql"
	"encoding/csv"
        "flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

const (
	concurrency = 100 // no > 100 (max postgres concurrent connections)
	rows = 1000 // rows per round
)

type customer struct {
	id    int    `json:"id""`
	name  string `json:"name""`
	email string `json:"email""`
}

func main() {
	now := time.Now()

	e := make(chan error)
	go func() {
		err := <- e
		if err != nil {
			log.Fatalln("error", err)
		}
	}()

	filepath := flag.String("f", "customers.csv", "Customer filepath")
        dbPassword := flag.String("p", "", "Postgres password")
	flag.Parse()

        if *dbPassword == "" {
        	log.Fatalln("Database password is required:\n    importer -p password")
	}

	connStr := fmt.Sprintf("host=localhost port=5432 dbname=postgres user=postgres password=%s sslmode=disable", *dbPassword)
        db, err := sql.Open("postgres", connStr)
	if err != nil {
		e <- err
	}
        defer db.Close()

	f, err := os.Open(*filepath)
	if err != nil {
		e <- err
	}
	defer f.Close()

	wg := &sync.WaitGroup{}

	workers := make(chan []*customer)

	for i := 1; i <= concurrency; i++ {
		wg.Add(1)
		go save(wg, db, workers, e)
	}

	customers := make([]*customer, 0, rows)
	rd := csv.NewReader(f)

	for {
		r, err := rd.Read()
		if err != nil {
			if err == io.EOF {
				break
			}

			e <- err
		}

		id, err := strconv.Atoi(r[0])
		if err != nil {
			e <- err
		}

		c := &customer{
			id:    id,
			name:  r[1],
			email: r[2],
		}

		customers = append(customers, c)

		if len(customers) == rows {
			workers <- customers
			customers = []*customer{}
		}

		log.Println(c)
	}

	if len(customers) > 0 {
		workers <- customers
	}

	close(workers)
	close(e)

	wg.Wait()

	log.Println(time.Since(now))
}

func save(wg *sync.WaitGroup, db *sql.DB, in chan []*customer, out chan error) {
	for cs := range in {
		tx, err := db.Begin()
		if err != nil {
			out <- err
		}

		sql := "INSERT INTO customers (id, name, email) VALUES ($1, $2, $3)"
		sql = sql + "ON CONFLICT (id) DO UPDATE "
		sql = sql + "SET name = EXCLUDED.name, email = EXCLUDED.email;"

		stmt, err := tx.Prepare(sql)
		if err != nil {
			out <- err
		}

		for _, c := range cs {
			_, err = stmt.Exec(c.id, c.name, c.email)
			if err != nil {
				out <- err
			}
		}

		tx.Commit()
	}

	wg.Done()
}